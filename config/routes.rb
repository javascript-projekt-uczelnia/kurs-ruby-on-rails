Rails.application.routes.draw do
  resources :forums

  get 'forum(/:page(/:tags))', to: 'forum#index', as: 'forum_index'
  post 'forum/search_by_tags', to: 'forum#search_by_tags', as: 'search_by_tags'
  post 'forum/search_by_page', to: 'forum#search_by_page', as: 'search_by_page'
  post 'forum/create_post', to: 'forum#create_post', as: 'create_post'
  post 'forum/toggle_post_like', to: 'forum#toggle_post_like', as: 'toggle_post_like'
  post 'forum/create_comment', to: 'forum#create_comment', as: 'create_comment'
  post 'forum/toggle_comment_like', to: 'forum#toggle_comment_like', as: 'toggle_comment_like'

  get 'login', to: 'login#index', as: 'login_index'
  post 'login', to: 'login#login'

  get 'register', to: 'register#index', as: 'register_index'
  post 'register', to: 'register#register'

  get 'user/:login', to: 'user#profile', as: 'user_profile'
  post 'user/logout', to: 'user#logout', as: 'user_logout'
  post 'user/changeAvatar', to: 'user#change_avatar'
  post 'user/changeDescription', to: 'user#change_description'
  post 'user/toggleFriend', to: 'user#toggle_friend', as: 'toggle_friend'

  get "up" => "rails/health#show", as: :rails_health_check

   root "forum#index"
end

Rails.application.routes.default_url_options[:host] = 'localhost:3000'
