Instalacja zależności:

- bundle install

Uruchomienie serwera:

- rails server

Migracje:

- rails generate migration CreateModel
- rails db:migrate
- rails db:schema:dump

Importy zewnętrzne:

- Bulma,
- FontAwesome
