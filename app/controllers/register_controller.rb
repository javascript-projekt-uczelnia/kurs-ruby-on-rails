class RegisterController < ApplicationController
    def index
        redirect_to forum_index_path if session[:user].present?
    end
  
    def show
    end
  
    def register
        login = params[:login]
        email = params[:email]
        password = params[:password]
        repeat_password = params[:repeat_password]

        if password.nil? || password.length < 3
            @error = "Hasło musi mieć co najmniej 3 znaki."
            render :index
            return
        end
    
        if User.exists?(login: login)
            @error = "Podany login już istnieje."
            render :index
            return
        end
    
        if email.blank?
              @error = "Nie podano adresu email."
            render :index
            return
        end
    
        if User.exists?(email: email)
            @error = "Podany email jest już zarejestrowany."
            render :index
            return
        end
    
        if password != repeat_password
            @error = "Błędnie powtórzono hasło."
            render :index
            return
        end
    
        user = User.new(login: login, email: email, password: password)
        user.created_at = Time.now
    
        if user.save
              session[:user] = { login: login }
              redirect_to forum_index_path
        else
              @error = "Nie udało się zarejestrować użytkownika."
              render :index
        end
      end
  end
  