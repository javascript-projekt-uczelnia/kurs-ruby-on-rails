$MAX_POSTS_PER_PAGE = 5

class ForumController < ApplicationController
  def get_posts_dto(page_id, tags)
    page_id = page_id >= 0 ? page_id : 0
    posts = Post.order(id: :desc)
  
    unless tags.blank?
      tags_array = tags.split(' ')
        .select { |tag| tag.length >= 3 }
        .take(10)
        .map { |tag| tag.length > 25 ? tag[0, 25] : tag }
        .uniq
  
      posts = posts.joins(:tags).where(tags: { name: tags_array })
    end
  
    posts = posts.offset(page_id * $MAX_POSTS_PER_PAGE).limit($MAX_POSTS_PER_PAGE)
    @posts = posts.map(&:to_dto)
  end
  
  
  def index
    @page = params[:page].to_i
    @tags = params[:tags]

    session[:page] = @page
    session[:tags] = @tags

    page_id = @page >= 0 ? @page : 0
    @posts_dto = get_posts_dto(page_id, @tags)
  end

  def show
  end

  def search_by_tags
    tags = params[:tags]
    choice = params[:choice].to_i
  
    if (choice == 0 && !tags.blank?)
      redirect_to action: "index", page: 0, tags: tags
    else
      redirect_to action: "index", page: 0, tags: ""
    end
  end

  def search_by_page
    page = params[:page].to_i
    tags = params[:tags].presence || ""
  
    redirect_to action: "index", page: (page.present? && page > 0 ? page : 0), tags: tags
  end
  

  def create_post
    unless session[:user].present?
      redirect_to login_index_path
      return
    end
  
    login = session[:user]['login']
    user = User.find_by(login: login)
  
    if user.nil?
      redirect_to login_index_path
      return
    end
  
    title = params[:title]
    content = params[:content]
    tags = params[:tags]
  
    if title.blank?
      @error_message = "Nie podano tytułu."
      redirect_to action: "index", page: session[:page], tags: session[:tags]
      return
    end
  
    if content.blank?
      @error_message = "Nie podano treści."
      redirect_to action: "index", page: session[:page], tags: session[:tags]
      return
    end
  
    post = Post.new
    post.title = title.length > 255 ? title[0, 255] : title
    post.content = content
    post.created_at = Time.now
    post.user_id = user.id
  
    unless tags.blank?
      tags_array = tags.split(' ').select { |tag| tag.length >= 3 }
        .take(10)
        .map { |tag| tag.length > 25 ? tag[0, 25] : tag }
        .uniq
  
      tags_array.each do |tag_name|
        new_tag = Tag.new(name: tag_name)
        post.tags << new_tag
      end
    end
  
    if post.save
      redirect_to action: "index", page: session[:page], tags: session[:tags]
    else
      @error_message = "Wystąpił błąd podczas zapisywania posta."
      redirect_to action: "index", page: session[:page], tags: session[:tags]
    end
  end  
  
  def toggle_post_like
    unless session[:user].present?
      redirect_to login_index_path
      return
    end
    
    login = session[:user]['login']
    user = User.find_by(login: login)
  
    if user.nil?
      redirect_to login_index_path
      return
    end
    
    post_id = params[:post_id]
    post = Post.find_by(id: post_id)
  
    if post.nil?
      redirect_to action: "index", page: session[:page], tags: session[:tags]
      return
    end
  
    if post.post_likes.exists?(user_id: user.id)
      post.post_likes.find_by(user_id: user.id).destroy
    else
      post.post_likes.create(user_id: user.id)
    end
    
    respond_to do |format|
      format.html { redirect_to action: "index", page: session[:page], tags: session[:tags] }
      format.json { render json: { post_id: post.id, likes: post.post_likes.count } }
      format.turbo_stream { render turbo_stream: turbo_stream.replace("post_like_count_#{post_id}", partial: "post_like_count", locals: { likes: post.post_likes.count }) }
    end
  end  

  def create_comment
    unless session[:user].present?
      redirect_to login_index_path
      return
    end
  
    login = session[:user]['login']
    user = User.find_by(login: login)
  
    if user.nil?
      redirect_to login_index_path
      return
    end
  
    post_id = params[:post_id]
    content = params[:content]
  
    if content.blank?
      @error_message = "Nie podano treści."
      redirect_to action: "index", page: session[:page], tags: session[:tags]
      return
    end

    comment = Comment.new
    comment.content = content
    comment.created_at = Time.now
    comment.user_id = user.id
    comment.post_id = post_id
  
    if comment.save
      redirect_to action: "index", page: session[:page], tags: session[:tags]
    else
      @error_message = "Wystąpił błąd podczas zapisywania komentarza."
      redirect_to action: "index", page: session[:page], tags: session[:tags]
    end
  end

  def toggle_comment_like
    unless session[:user].present?
      redirect_to login_index_path
      return
    end
    
    login = session[:user]['login']
    user = User.find_by(login: login)
  
    if user.nil?
      redirect_to login_index_path
      return
    end
    
    comment_id = params[:comment_id]
    comment = Comment.find_by(id: comment_id)
  
    if comment.nil?
      redirect_to action: "index", page: session[:page], tags: session[:tags]
      return
    end
  
    if comment.comment_likes.exists?(user_id: user.id)
      comment.comment_likes.find_by(user_id: user.id).destroy
    else
      comment.comment_likes.create(user_id: user.id)
    end
    
    redirect_to action: "index", page: session[:page], tags: session[:tags]
  end
end
  