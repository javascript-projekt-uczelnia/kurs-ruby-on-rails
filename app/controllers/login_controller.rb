class LoginController < ApplicationController
  def index
    redirect_to forum_index_path if session[:user].present?
  end

  def show
  end

  def login
    login = params[:login]
    password = params[:password]
    user = User.find_by(login: login)

    if user && user.password == password
      session[:user] = { login: login }
      redirect_to root_path
    else
      @error = "Niepoprawny login lub hasło."
      render :index
    end
  end
end
