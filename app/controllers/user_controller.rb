class UserController < ApplicationController
  def profile
    @login = params[:login]
    user = User.find_by(login: @login)

    if user.present?
      @profile = UserProfile.new(
        login: user.login,
        created_at: user.created_at,
        avatar_url: user.avatar.attached? ? rails_blob_url(user.avatar) : nil,
        description: user.description,
        posts_count: user.posts.count,
        comments_count: user.comments.count,
        likes_count: user.post_likes.count + user.comment_likes.count,
        is_my_friend: is_my_friend(user)
      )

      if session[:user].present? && session[:user]['login'] == user.login
        @profile.friends = user.friends
      end
    else
      @profile = nil
    end
  end

  def change_avatar
    unless session[:user].present?
      redirect_to login_index_path
      return
    end
    
    login = session[:user]['login']
    user = User.find_by(login: login)
  
    return redirect_to login_index_path if user.nil?
  
    avatar = params[:avatar]
  
    if avatar.present?
      user.avatar.attach(avatar)
    end
  
    redirect_to action: "profile", login: user.login
  end   

  def change_description
    unless session[:user].present?
      redirect_to login_index_path
      return
    end
    
    login = session[:user]['login']
    user = User.find_by(login: login)
    
    return redirect_to login_index_path if user.nil?
    
    new_description = params[:description]
    user.update(description: new_description)
    
    redirect_to action: "profile", login: user.login
  end
  
  def toggle_friend
    p 'test'
    unless session[:user].present?
      redirect_to login_index_path
      return
    end
    
    friend_login = params[:login]
    friend = User.find_by(login: friend_login)

    return redirect_to login_index_path if friend.nil?

    current_user = User.find_by(login: session[:user]['login'])

    if current_user.friends.include?(friend)
      current_user.friends.delete(friend)
    else
      current_user.friends << friend
    end

    redirect_to action: "profile", login: friend_login
  end

  def logout
    session.delete(:user) if session[:user].present?
    redirect_to forum_index_path
  end

  private

  def is_my_friend(user)
    return false unless session[:user].present?
    return false if session[:user]['login'] == user.login

    current_user = User.find_by(login: session[:user]['login'])
    return current_user.friends.include?(user)
  end
end
