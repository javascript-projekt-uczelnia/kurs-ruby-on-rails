class Post < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :tags
  
  has_many :comments
  has_many :post_likes

  validates :title, presence: true, length: { maximum: 255 }
  validates :content, presence: true

  before_save :set_created_at

  def to_dto
    return PostDto.new(
      id: self.id,
      user_login: self.user.login,
      avatar_url: self.user.get_avatar_url,
      tags: self.tags,
      comments: self.comments.map(&:to_dto),
      likes: self.post_likes.count,
      title: self.title,
      content: self.content,
      created_at: self.created_at
    )
  end

  private

  def set_created_at
    self.created_at ||= Time.now
  end
end