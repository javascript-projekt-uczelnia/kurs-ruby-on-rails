class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post
  has_many :comment_likes

  validates :content, presence: true
  before_save :set_created_at

  def to_dto
    return CommentDto.new(
      id: self.id,
      user_login: self.user.login,
      avatar_url: self.user.get_avatar_url,
      likes: self.comment_likes.count,
      content: self.content,
      created_at: self.created_at
    )
  end

  private
  def set_created_at
    self.created_at ||= Time.now
  end
end