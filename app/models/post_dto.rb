class PostDto
  attr_accessor :id
  attr_accessor :user_login
  attr_accessor :avatar_url
  attr_accessor :tags
  attr_accessor :comments
  attr_accessor :likes
  attr_accessor :title
  attr_accessor :content
  attr_accessor :created_at

  def initialize(attributes = {})
    @id = attributes[:id]
    @user_login = attributes[:user_login]
    @avatar_url = attributes[:avatar_url]
    @tags = attributes[:tags]
    @comments = attributes[:comments]
    @likes = attributes[:likes]
    @title = attributes[:title]
    @content = attributes[:content]
    @created_at = attributes[:created_at]
  end
end