class CommentDto
  attr_accessor :id
  attr_accessor :user_login
  attr_accessor :avatar_url
  attr_accessor :content
  attr_accessor :likes
  attr_accessor :created_at

  def initialize(attributes = {})
    @id = attributes[:id]
    @user_login = attributes[:user_login]
    @avatar_url = attributes[:avatar_url]
    @content = attributes[:content]
    @likes = attributes[:likes]
    @created_at = attributes[:created_at]
  end
end
