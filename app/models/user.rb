include Rails.application.routes.url_helpers
class User < ApplicationRecord
    has_many :posts
    has_many :comments
    has_many :post_likes
    has_many :comment_likes
  
    validates :login, presence: true, length: { maximum: 30 }
    validates :email, presence: true, length: { maximum: 50 }, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :password, presence: true, length: { maximum: 100 }
    has_one_attached :avatar
    validates :description, length: { maximum: 3000 }, allow_blank: true
    
    before_save :set_created_at
    
    has_many :friendships
    has_many :friends, through: :friendships

    def get_avatar_url
      return self.avatar.attached? ? rails_blob_url(self.avatar) : nil
    end

    private
    def set_created_at
      self.created_at ||= Time.now
    end
  end
  