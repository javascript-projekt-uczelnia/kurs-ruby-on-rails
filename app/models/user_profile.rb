class UserProfile
  include ActiveModel::Model
  attr_accessor :login, :created_at, :avatar_url, :description
  attr_accessor :posts_count, :comments_count, :likes_count
  attr_accessor :is_my_friend
  attr_accessor :friends

  validates :login, presence: true
end
